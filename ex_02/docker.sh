#!/bin/bash

docker run -d --name server1 -v myvolume:/usr/share/nginx/html nginx:latest

docker run -d --name server2 -v myvolume:/usr/share/nginx/html nginx:latest

docker run -d --name server-debian -v myvolume:/usr/share/nginx/html debian-vim:1.0
